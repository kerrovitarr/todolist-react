import React from "react";
import "./App_styles.css";

function Task({ task }) {
  return (
    <div className="Task">
      <li className={task.isDone ? "task-done" : ""}>
        <input
          type="checkbox"
          onClick={(e) => {
            e.preventDefault();
            return false;
          }}
          readOnly="readonly"
          checked={task.isDone}
        />
        {task.text}
      </li>
    </div>
  );
}

export default Task;

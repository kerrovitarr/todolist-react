import React, { useState } from "react";
import Context from "./context";

function EditNote() {
  const { modalState, setModalState } = React.useContext(Context);
  const [value, setValue] = useState(modalState.note.text);
  const [tasks, setTasks] = useState(Object.assign([], modalState.note.tasks));
  return (
    <div className="edit-note-container">
      <div className="edit-note-title">
        <span>Заголовок: </span>
        <input
          value={value}
          onChange={(event) => {
            event.persist();
            setValue(event.target.value);
            setModalState((prevState) => ({
              ...prevState,
              note: { ...prevState.note, text: event.target.value },
            }));
          }}
        />
      </div>
      <div className="edit-note-tasks">
        <span>Задачи:</span>
        <div>
          {tasks.map((taskItem) => {
            return (
              <div key={taskItem.id} className="edit-note-task-row">
                <input
                  type="checkbox"
                  value={taskItem.isDone}
                  onChange={(e) => {
                    e.persist();
                    taskItem.isDone = !taskItem.isDone;
                    setTasks(
                      tasks.map((taskItemState) => {
                        if (taskItemState.id === taskItem.id)
                          taskItemState.isDone = taskItem.isDone;
                        return taskItemState;
                      })
                    );
                  }}
                  defaultChecked={taskItem.isDone}
                />
                <input
                  value={taskItem.text}
                  onChange={(event) => {
                    event.persist();
                    setTasks(
                      tasks.map((taskItemState) => {
                        if (taskItemState.id === taskItem.id)
                          taskItemState.text = event.target.value;
                        return taskItemState;
                      })
                    );
                  }}
                />
                <div
                  className="btn delete-btn delete-task-btn"
                  onClick={(e) => {
                    e.persist();
                    setModalState((prevState) => ({
                      ...prevState,
                      note: {
                        ...prevState.note,
                        tasks: prevState.note.tasks.filter(
                          (taskItemState) => taskItemState.id !== taskItem.id
                        ),
                      },
                    }));
                  }}
                >
                  &times;
                </div>
              </div>
            );
          })}
          <div
            className="btn primary-btn add-task-btn"
            onClick={(e) => {
              e.persist();
              let newTask = { id: Date.now(), text: "", isDone: false };
              setTasks(tasks.concat(newTask));
              setModalState((prevState) => ({
                ...prevState,
                note: {
                  ...prevState.note,
                  tasks: prevState.note.tasks.concat(newTask),
                },
              }));
            }}
          >
            Создать задачу
          </div>
        </div>
      </div>
    </div>
  );
}

export default EditNote;

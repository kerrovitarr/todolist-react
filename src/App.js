import React, { useEffect } from "react";
import Note from "./Note";
import Context from "./context";
import Modal from "./Modal/Modal";
import AlertBox from "./AlertBox";
import "./App_styles.css";

function App() {
  const [alertState, setAlertState] = React.useState({text: '', isVisible: false, x:0,y:0});
  const [modalState, setModalState] = React.useState({
    type: "",
    state: false,
    note: null,
  });

  let loadedNotes = JSON.parse(sessionStorage.getItem('notes'));
  loadedNotes = (loadedNotes === null) ? [
    {
      id: 1,
      text: "Первая заметка",
      tasks: [
        { id: 1, text: "Задача 1.1", isDone: true },
        { id: 2, text: "Задача 1.2", isDone: false },
        { id: 3, text: "Задача 1.3", isDone: false },
      ],
    },
    {
      id: 2,
      text: "Вторая заметка",
      tasks: [
        { id: 1, text: "Задача 2.1", isDone: true },
        { id: 2, text: "Задача 2.2", isDone: true },
        { id: 3, text: "Задача 2.3", isDone: false },
      ],
    },
    {
      id: 3,
      text: "Третья заметка",
      tasks: [
        { id: 1, text: "Задача 3.1", isDone: true },
        { id: 2, text: "Задача 3.2", isDone: true },
        { id: 3, text: "Задача 3.3", isDone: true },
      ],
    },
  ] : loadedNotes;
  const [notes, setNotes] = React.useState(loadedNotes);

  function deleteNote() {
    setNotes(notes.filter((noteItem) => noteItem.id !== modalState.note.id));
  }

  function addNote() {
    setNotes(notes.concat([Object.assign({}, modalState.note)]));
  }

  function changeNote() {
    setNotes(
      notes.map((noteItem) => {
        if (noteItem.id === modalState.note.id)
          Object.assign(noteItem, modalState.note);
        return noteItem;
      })
    );
  }

  function openModal(modalType, note = null) {
    //avoid copying nested array of objects as reference by stringify -> parse
    let noteDeepClone = JSON.parse(JSON.stringify(note));
    setModalState({
      type: modalType,
      state: true,
      note:
        modalType === "addNote"
          ? { id: Date.now(), text: "", tasks: [] }
          : noteDeepClone,
    });
  }

  function showAlertText(text) {
    let rect = document.getElementsByClassName('modal-header')[0].getBoundingClientRect();
    let x = rect.x;
    let y = rect.y - 41;
    setAlertState({text: text, isVisible: true, x: x, y: y});
    setTimeout(() => {setAlertState((prevState) => ({...prevState, isVisible:false}))},4000);
  }

  useEffect(() => {
    sessionStorage.setItem("notes", JSON.stringify(notes));
    if (modalState.state === false && alertState.isVisible === true) {
      setAlertState((prevState) => ({...prevState, isVisible:false}));
    }
  });

  return (
    <Context.Provider value={{ modalState, setModalState }}>
      <div className="App">
        {alertState.isVisible && <AlertBox x={alertState.x} y={alertState.y} text={alertState.text} />}
        <Modal
          deleteNote={deleteNote}
          addNote={addNote}
          changeNote={changeNote}
          showAlertText={showAlertText}
        />
        <h1>Заметки</h1>
        <ol>
          {notes.map((noteItem) => {
            return (
              <Note
                note={noteItem}
                openModal={openModal}
                key={noteItem.id}
              ></Note>
            );
          })}
        </ol>
        <div
          className="btn primary-btn add-note-btn"
          onClick={() => openModal("addNote")}
        >
          Создать заметку
        </div>
      </div>
    </Context.Provider>
  );
}

export default App;

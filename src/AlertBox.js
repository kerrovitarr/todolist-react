import React from "react";

function AlertBox({text, x, y}) {
  return (
    <div id="alertBox" className="alert-box" style={{'top':y,'left':x}}>
      <span>{text}</span>
    </div>
  );
}

export default AlertBox
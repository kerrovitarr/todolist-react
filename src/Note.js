import React from "react";
import Task from "./Task";
import "./App_styles.css";

function Note({ note, openModal }) {
  return (
    <li>
      <div className="note">
        <div>{note.text}</div>
        <div>
          <div
            className="btn primary-btn"
            onClick={() => openModal("changeNote", note)}
          >
            Изменить
          </div>
          <div
            className="btn delete-btn"
            onClick={() => openModal("deleteNote", note)}
          >
            Удалить
          </div>
        </div>
      </div>
      <ul>
        {note.tasks.map((taskItem) => {
          return <Task task={taskItem} key={taskItem.id}></Task>;
        })}
      </ul>
    </li>
  );
}

export default Note;

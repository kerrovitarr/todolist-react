import React from "react";
import "./Modal_style.css";
import Context from "../context";
import EditNote from "../EditNote";

function Modal({ deleteNote, addNote, changeNote, showAlertText }) {
  const { modalState, setModalState } = React.useContext(Context);
  let modalSettings = { title: "", body: "", actionBtn: "", note: null };
  switch (modalState.type) {
    case "addNote":
      modalSettings = {
        title: "Создать заметку",
        body: <EditNote />,
        actionBtn: "Добавить",
      };
      break;
    case "changeNote":
      modalSettings = {
        title: "Изменить заметку",
        body: <EditNote note={modalState.note} />,
        actionBtn: "Сохранить",
        note: modalState.note,
      };
      break;
    case "deleteNote":
      modalSettings = {
        title: "Подтверждение удаления",
        body: (
          <div>
            Вы уверены что хотите удалить заметку с заголовком:
            <br />
            <i>
              <b>{modalState.note.text}</b>
            </i>
          </div>
        ),
        actionBtn: "Удалить",
        note: modalState.note,
      };
      break;
    default:
      break;
  }

  if (modalState.state)
    return (
      <div className="modal">
        <div className="modal-content">
          <div className="modal-header">
            <div>{modalSettings.title}</div>
            <div
              id="close-modal-btn"
              onClick={() => {
                setModalState((prevState) => ({ ...prevState, state: false }));
              }}
            >
              &times;
            </div>
          </div>
          <div className="modal-body">{modalSettings.body}</div>
          <div className="modal-footer">
            <div
              className="btn cancel-btn"
              onClick={() => {
                setModalState((prevState) => ({ ...prevState, state: false }));
              }}
            >
              Отмена
            </div>
            <div
              className={
                modalState.type === "deleteNote"
                  ? "btn delete-btn"
                  : "btn success-btn"
              }
              onClick={(e) => {
                if (
                  confirmAction(deleteNote, addNote, changeNote, modalState, showAlertText, e)
                ) {
                  setModalState((prevState) => ({
                    ...prevState,
                    state: false,
                  }));
                }
              }}
            >
              {modalSettings.actionBtn}
            </div>
          </div>
        </div>
      </div>
    );
  else return null;
}

function confirmAction(deleteNote, addNote, changeNote, modalState, showAlertText, e) {
  let haveAnyInputEmpty = false;
  if (modalState.note.text.trim().length === 0) haveAnyInputEmpty = true;
  else {
    modalState.note.tasks.forEach((task) => {
      if (task.text.trim().length === 0) haveAnyInputEmpty = true;
    });
  }
  if (haveAnyInputEmpty) {
    showAlertText("Заголовок заметки и описание любой задачи не могут быть пустыми!", e);
    return false;
  } else {
    switch (modalState.type) {
      case "addNote":
        addNote();
        break;
      case "changeNote":
        changeNote();
        break;
      case "deleteNote":
        deleteNote();
        break;
      default:
        break;
    }
    return true;
  }
}

export default Modal;
